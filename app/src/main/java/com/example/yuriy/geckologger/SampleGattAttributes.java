/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.yuriy.geckologger;

import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class SampleGattAttributes {
    private static HashMap<String, String> attributes = new HashMap();
    public static String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    static {
        // Sample Services.
        attributes.put("0000180d-0000-1000-8000-00805f9b34fb", "Heart Rate Service");
        attributes.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");
        attributes.put("00001800-0000-1000-8000-00805f9b34fb", "GAP Service");
        attributes.put("104a3fdf-9a37-42da-897a-10c85402f201", "Control Service");
        attributes.put("104a3fdf-9a37-42da-897a-10c85402f202", "Data Service");
        // Sample Characteristics.
        attributes.put(HEART_RATE_MEASUREMENT, "Heart Rate Measurement");
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");

        attributes.put("419de4aa-c0e6-4c4c-a0b8-2812ebc7f401", "Device parameters");
        attributes.put("419de4aa-c0e6-4c4c-a0b8-2812ebc7f402", "Device control");
        attributes.put("419de4aa-c0e6-4c4c-a0b8-2812ebc7f403", "Coefficients");

        attributes.put("35c75d3c-04bb-4aec-85b2-cfbcd8057c01", "PSU data");
        attributes.put("35c75d3c-04bb-4aec-85b2-cfbcd8057c02", "Engine data");
        attributes.put("35c75d3c-04bb-4aec-85b2-cfbcd8057c03", "Accelerometer data");
        attributes.put("35c75d3c-04bb-4aec-85b2-cfbcd8057c04", "Temperature data");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
